FROM python:3.8

RUN apt-get update
RUN apt-get install git

WORKDIR /srv

COPY ./ ./

COPY ./requirements.txt ./

RUN pip install -r requirements.txt

CMD ["python", "manage.py", "runserver", "0:8000"]
